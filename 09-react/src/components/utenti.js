import { useEffect, useState } from "react";
import Utente from "./utente";

function Utenti() {
    let [users, setUsers] = useState([]);

    useEffect(() => {
        console.info("useEffect");
        const url = "https://jsonplaceholder.typicode.com/users/";
        fetch(url)
            .then(res => res.json())
            .then(data => setUsers(data))
    }, []);

    useEffect(() => console.info("useEffect users", users), [users]);

    const deleteUser = (id) => setUsers(users.filter(u => u.id!==parseInt(id)))

    return (
        <div>
            <h1>Utenti</h1>
            <ul>
                {
                    users.map(user => <Utente user={user} deleteUser={(id) => deleteUser(id)}></Utente>)
                }
            </ul>
        </div>
    );
}

export default Utenti;