function Utente({ user, deleteUser }) {
    return (
        <li key={user.id}>
            {user.name}
            <button onClick={deleteUser}>X</button>
        </li>
    );
}

export default Utente;