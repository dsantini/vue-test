import logo from './logo.svg';
import './App.css';
import { useEffect, useState, Component } from "react";
import Utenti from './components/utenti';

/**
 * Componenete presentazionale (stateless) funzionale
 * Prima e dopo di React 16.8
 */
function Titolo() {
  return (
    <> {/* <== "Fragment" */}
      <h1>Test Componente JSX funzione</h1>
      <h2>Sottotitolo</h2>
    </>
  );
}

/**
 * Componente stateful funzionale usando i React hooks
 * Solo dopo di React 16.8
 */
function Contatore2() {
  let [contatore, setContatore] = useState(0);

  const incrementa = () => setContatore(contatore + 1);

  const decrementa = () => setContatore(contatore - 1);

  useEffect(() => console.info("useEffect"));

  useEffect(() => console.info("useEffect contatore", contatore), [contatore]);

  return (
    <div>
      <h1>Contatore funzionale</h1>
      <h2>{contatore}</h2>
      <button onClick={incrementa}>++</button>
      <button onClick={decrementa}>--</button>
    </div>
  );
}

/**
 * Componente stateful object oriented
 * Prima e dopo di React 16.8
 * Sta andando in disuso lasciando spazio agli hooks
 */
class Contatore extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contatore: 0
    };
  }

  incrementa = () => {
    this.setState(state => {
      return { contatore: state.contatore + 1 };
    });
  }

  decrementa = () => {
    this.setState(state => {
      return { contatore: state.contatore - 1 };
    });
  }

  componentDidMount() {
    console.info("componentDidMount");
  }

  componentDidUpdate() {
    console.info("componentDidUpdate");
  }

  render() {
    return (
      <div>
        <h1>Contatore OOP</h1>
        <h2>{this.state.contatore}</h2>
        <button onClick={this.incrementa}>++</button>
        <button onClick={this.decrementa}>--</button>
      </div>
    );
  }
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Titolo></Titolo>
        <Contatore></Contatore>
        <Contatore2></Contatore2>
        <Utenti></Utenti>
      </header>
    </div>
  );
}

export default App;
