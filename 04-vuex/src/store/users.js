export const usersStore = {
    state: {
        users: {
            data: [],
            error: false
        }
    },
    getters: {
        getUsers(state) {
            return state.users.data;
        },
        getNUsers(state, num) {
            return state.users.data.filter((el, n) => n < num);
        }
    },
    mutations: {
        setUsers(state, users) {
            state.users = { data: users, error: false };
        },
        fetchUsersSuccess(state, users) {
            state.users = { data: users, error: false };
        },
        fetchUsersFailure(state) {
            state.users = { data: [], error: true };
        }
    },
    actions: { // Side effects
        async fetchUsers( {commit} ) {
            try {
                const url = "https://jsonplaceholder.typicode.com/users",
                    response = await fetch(url),
                    responseContent = await response.json();
                commit("fetchUsersSuccess", responseContent);
            } catch (e) {
                commit("fetchUsersFailure");
            }
        }
    },
};