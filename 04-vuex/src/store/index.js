import { createStore } from 'vuex'
import { countStore } from './count.js'
import { usersStore } from './users.js'

export default createStore({
  state: {
    
  },
  getters: {
    
  },
  mutations: {
    
  },
  actions: { // Side effects

  },
  modules: {
    count: countStore,
    users: usersStore
  }
})
