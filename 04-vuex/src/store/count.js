export const countStore = {
    state: {
        count: 0
    },
    getters: {
        getCount(state) {
            return state.count;
        }
    },
    mutations: {
        incrementCount(state, increment = 1) {
            state.count += increment;
        },
        decrementCount(state, decrement = 1) {
            state.count -= decrement;
        },
        setCount(state, count) {
            state.count = count;
        }
    },
    actions: { // Side effects
        async fetchCount(context) {
            const url = "https://www.random.org/integers/?num=1&min=1&max=6&col=1&base=10&format=plain&rnd=new",
                response = await fetch(url),
                responseContent = await response.json();
            context.commit("setCount", responseContent);
        }
    },
};