export const productsStore = {
  state: {
    products: {
      data: [],
      error: "Non inizializzato"
    }
  },
  getters: {
    getProducts(state) {
      return state.products.data;
    },
    getProductsError(state) {
      return state.products.error;
    },
    getProductByID(state) {
      return (id) => state.products.data.find((el) => el.id === parseInt(id));
    },
    getProductsByCategory(state) {
      return (category) => {
        console.info("getProductsByCategory", state, category);
        return state.products.data.filter((el) => el.category_id === parseInt(category?.id));
      };
    },
  },
  mutations: {
    removeProduct(state, category) {
      state.products = {
        ...state.products,
        data: state.products.data.filter((el) => el.id !== category?.id)
      };
    },
    setProducts(state, products) {
      state.products = { data: products, error: false };
    },
    setProductsError(state, error) {
      state.products = { data: [], error: error }
    }
  },
  actions: {
    loadProductsSuccess({ commit }, products) {
      commit("setProducts", products);
    },
    loadProductsFailure({ commit }, message) {
      commit("setProductsError", message);
    },
    async loadProducts({ dispatch }) {
      console.info("loadProducts");
      try {
        const url = "http://localhost:3000/products",
          response = await fetch(url),
          responseContent = await response.json();
        dispatch("loadProductsSuccess", responseContent);
      } catch (e) {
        dispatch("loadProductsFailure", e.message);
      }
    },
    loadProductsIfNecessary({ getters, dispatch }) {
      console.info("loadProductsIfNecessary");
      if (getters.getProductsError !== false) {
        dispatch("loadProducts");
      }
    },
  },
};
