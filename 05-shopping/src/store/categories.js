export const categoriesStore = {
  state: {
    categories: {
      data: [],
      error: "Non inizializzato"
    }
  },
  getters: {
    getCategories(state) {
      return state.categories.data;
    },
    getCategoriesError(state) {
      return state.categories.error;
    },
    getCategoryByID(state) {
      return (id) => state.categories.data.find((el) => el.id === parseInt(id));
    },
  },
  mutations: {
    removeCategory(state, category) {
      state.categories = {
        ...state.categories,
        data: state.categories.data.filter((el) => el.id !== parseInt(category?.id))
      };
    },
    setCategories(state, categories) {
      state.categories = { data: categories, error: false };
    },
    setCategoriesError(state, error) {
      state.categories = { data: [], error: error }
    }
  },
  actions: {
    loadCategoriesSuccess({ commit }, categories) {
      commit("setCategories", categories);
    },
    loadCategoriesFailure({ commit }, message) {
      commit("setCategoriesError", message);
    },
    async loadCategories({ dispatch }) {
      console.info("loadCategories");
      try {
        const url = "http://localhost:3000/categories",
          response = await fetch(url),
          responseContent = await response.json();
        dispatch("loadCategoriesSuccess", responseContent);
      } catch (e) {
        dispatch("loadCategoriesFailure", e.message);
      }
    },
  },
};
