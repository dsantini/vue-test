export const cartStore = {
  state: {
    cartItems: {
      data: [],
      error: "Non inizializzato"
    }
  },
  getters: {
    getCartItems(state) {
      return state.cartItems.data;
    },
    getCartError(state) {
      return state.cartItems.error;
    },
    getCartItemByID(state) {
      return (id) => state.cartItems.data.find((el) => el.id === parseInt(id));
    },
    getCartItemByProduct(state) {
      return (product) => state.cartItems.data.find((el) => el.product_id === parseInt(product?.id));
    },
    getCartTotalPrice(state) {
      return state.cartItems.data.map((el) => el.price * el.qty).reduce((old_sum, new_price) => old_sum + new_price, 0);
    },
    isProductInCart(state) {
      return (product) => state.cartItems.data.some((item) => item.product_id === parseInt(product?.id));
    },
  },
  mutations: {
    removeItemFromCart(state, item) {
      state.cartItems = {
        ...state.cartItems,
        data: state.cartItems.data.filter((el) => el.id !== parseInt(item?.id))
      };
    },
    setCartItems(state, cartItems) {
      state.cartItems = { data: cartItems, error: false };
    },
    setCartError(state, error) {
      state.cartItems = { data: [], error: error }
    },
    incrementItemCountInCart(state, { item, n }) {
      const increment = parseInt(n),
        itemCount = state.cartItems.data.find((el) => el.id === parseInt(item?.id))?.qty;
      console.info("incrementItemCountInCart", { item, n, increment, itemCount });

      let newData;
      if (increment < 0 && itemCount <= 1) { // Da eliminare
        newData = state.cartItems.data.filter((el) => el.id !== parseInt(item?.id));
      } else {
        newData = state.cartItems.data.map((el) => el.id !== parseInt(item?.id) ? el : { ...el, qty: el.qty + increment });
      }

      state.cartItems = { ...state.cartItems, data: newData };
    },
    addNewProductToCart(state, { product, n }) {
      const itemCount = parseInt(n),
        newID = Math.min(0, ...state.cartItems.data.map((item) => item.id)) - 1,
        newItem = {
          product_id: product.id,
          qty: itemCount,
          title: product.title,
          price: product.price,
          image: product.image,
          id: newID,
        };
      console.info("addNewProductToCart", { product, n, itemCount, newItem });
      state.cartItems = {
        ...state.cartItems,
        data: [...state.cartItems.data, newItem],
      };
    },
    removeAllCartItems(state) {
      state.cartItems = { data: [], error: false }
    },
  },
  actions: {
    loadCartSuccess({ commit }, cartItems) {
      commit("setCartItems", cartItems);
    },
    loadCartFailure({ commit }, message) {
      commit("setCartError", message);
    },
    async loadCart({ dispatch }) {
      console.info("loadCart");
      try {
        const url = "http://localhost:3000/cart",
          response = await fetch(url),
          responseContent = await response.json();
        dispatch("loadCartSuccess", responseContent);
      } catch (e) {
        dispatch("loadCartFailure", e.message);
      }
    },
    loadCartIfNecessary({ getters, dispatch }) {
      console.info("loadCartIfNecessary");
      if (getters.getCartError !== false) {
        dispatch("loadCart");
      }
    },
    addProductToCart({ getters, commit }, { product, n }) {
      const existingItem = getters.getCartItemByProduct(product);
      console.info("addProductToCart", { product, n, existingItem });

      if (existingItem)
        commit("incrementItemCountInCart", { existingItem, n });
      else
        commit("addNewProductToCart", { product, n });
    },
  },
};
