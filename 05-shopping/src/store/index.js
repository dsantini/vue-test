import { createStore } from "vuex";
import { cartStore } from "./cart.js";
import { categoriesStore } from "./categories.js";
import { productsStore } from "./products.js";

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    cart: cartStore,
    categories: categoriesStore,
    products: productsStore,
  },
});
