# Test Vue.js

## NPM lite-server
[00-import](00-import/)
<details><summary>Inizializzazione</summary>

```sh
npm init
npm i lite-server
# start: "lite-server"
npm start
```

</details>

## Preparazione all'utilizzo di Vue

```sh
npm install @vue/cli --global
vue -V
```

## Estensione Chrome/Edge Dev Tools per Vue 3

https://chrome.google.com/webstore/detail/vuejs-devtools/ljjemllljcmogpfapbkkighbhhppjdbg

## Vue CLI
[02-cli](02-cli/)
<details><summary>Inizializzazione</summary>

```sh
vue create 02-cli
# Vue 3
cd 02-cli
# 02-cli/package.json: serve => "vue-cli-service serve --open"
npm run serve
```
(Per risolvere l'avviso di VS Code aprire direttamente la cartella [02-cli](02-cli/) e creare [jsconfig.json](02-cli/jsconfig.json))

</details>

## Vue CLI + vue-router
[03-routing](03-routing/)
<details><summary>Inizializzazione</summary>

```sh
vue create 03-routing
# Vue 3
cd 03-routing
vue add router
# History mode: Yes
# 03-routing/package.json: serve => "vue-cli-service serve --open"
npm run serve
```

</details>

## Vue CLI + vuex
[04-vuex](04-vuex/)
<details><summary>Inizializzazione</summary>

```sh
vue create 04-vuex
# Vue 3
cd 04-vuex
vue add vuex
# History mode: Yes
# 04-vuex/package.json: serve => "vue-cli-service serve --open"
npm run serve
```

</details>

## Vue shopping
[05-shopping](05-shopping/)
<details><summary>Inizializzazione</summary>

```sh
vue create 05-shopping
# Manually
# router, vuex
# Vue 3
# History mode: Yes
cd 05-shopping
npm i json-server -D
# server/db.json
# 05-shopping/package.json: serve => "vue-cli-service serve --open"
# 05-shopping/package.json: server => "json-server --watch server/db.json"
npm run server & npm run serve
```

</details>

<!-- TODO: json-server-auth -->

## Vue Composition API
[06-composition](06-composition/)
<details><summary>Inizializzazione</summary>

```sh
vue create 06-composition
# Vue 3
cd 06-composition
# 06-composition/package.json: serve => "vue-cli-service serve --open"
npm run serve
```
(Per risolvere l'avviso di VS Code aprire direttamente la cartella [06-composition](06-composition/) e creare [jsconfig.json](06-composition/jsconfig.json))

</details>

## Typescript
[07-typescript](07-typescript/)
<details><summary>Inizializzazione</summary>

```sh
mkdir 07-typescript
# Vue 3
cd 07-typescript
npm i typescript lite-server concurrently -D
# 07-typescript/package.json: server => "lite-server"
# 07-typescript/package.json: tsc => "tsc -w"
# 07-typescript/tsconfig.json
# 07-typescript/package.json: start => "concurrently 'npm:tsc' 'npm:server'"
npm run start
```

</details>

## Vue Typescript
[08-vue-ts](08-vue-ts/)
<details><summary>Inizializzazione</summary>

```sh
vue create 08-vue-ts
# Manually
# typescript, router, vuex
# Vue 3
# class-style syntax Yes, babel Yes, History mode Yes
cd 08-vue-ts
npm i json-server concurrently axios bootstrap -D
# server/db.json
# 08-vue-ts/package.json: serve => "vue-cli-service serve --open"
# 08-vue-ts/package.json: server => "json-server --watch server/db.json"
# 08-vue-ts/package.json: start => "concurrently 'npm:server' 'npm:serve'"
npm run start
```

</details>

## React
[09-react](09-react/)
<details><summary>Inizializzazione</summary>

```sh
# Se non si usa da molto: npx clear-npx-cache
npx create-react-app 09-react
cd 09-react
npm run start
```

</details>

## Angular
[10-angular](10-angular)
<details><summary>Inizializzazione</summary>

```sh
npm i -g @angular/cli
ng new 10-angular
cd 10-angular
npm run start
```

</details>
<details><summary>Generazione automatica componenti</summary>

```sh
ng generate component features/Contatore
# oppure ng g c features/Contatore
# se non si vuole il .html separato: --inline-template
# se non si vuole il .css separato: --inline-style
# se non si vogliono i test: --skip-tests
# se non si vuole la cartella a parte: --flat
```

</details>

## Vue internationalization
[11-i18n](11-i18n/)
<details><summary>Inizializzazione</summary>

```sh
vue create 11-i18n
# Vue 3
cd 11-i18n
vue add i18n
# 11-i18n/package.json: serve => "vue-cli-service serve --open"
npm run serve
```

</details>
