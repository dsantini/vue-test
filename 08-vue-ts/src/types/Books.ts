export default interface Book {
  id: number;
  title: string;
  description: string;
  published: boolean;
}
