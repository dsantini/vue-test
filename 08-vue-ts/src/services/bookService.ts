import http from "../http-common";

class BookService {
  getAll(): Promise<any> {
    return http.get("/books");
  }
}

export default new BookService();
