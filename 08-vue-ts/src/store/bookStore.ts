import Book from "@/types/Books";

const bookStore = {
  state: {
    books: [] as Book[],
  },
  mutations: {
    setBooks(state:any, books: Book[]): void {
      state.books = books;
    },
  },
};

export default bookStore;
