import { createStore } from "vuex";
import bookStore from "./bookStore";

export default createStore({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    book: bookStore,
  },
});
