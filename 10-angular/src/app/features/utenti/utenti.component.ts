import { Component, Inject, Injectable, OnInit } from '@angular/core';
import User from 'src/app/core/user';
import { UserService } from 'src/app/core/user.service';

@Component({
  selector: 'app-utenti',
  templateUrl: './utenti.component.html',
  styleUrls: ['./utenti.component.scss']
})
export class UtentiComponent implements OnInit {
  users: User[] = [];

  constructor(private userService: UserService) {}

  deleteUser(id:any): void {
    this.users = this.users.filter(u => u.id !== parseInt(id));
  }

  ngOnInit(): void {
    console.info("ngOnInit");
    this.userService.getUsers()
      .subscribe(data => this.users = data);
  }

}
