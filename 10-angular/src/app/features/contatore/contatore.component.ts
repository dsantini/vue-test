import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contatore',
  templateUrl: './contatore.component.html',
  styleUrls: ['./contatore.component.scss']
})
export class ContatoreComponent implements OnInit {
  contatore = 0;

  constructor() { }

  ngOnInit(): void {
  }

  incrementa() {
    this.contatore++;
  }

  decrementa() {
    this.contatore--;
  }
}
