import { Injectable } from '@angular/core';
import User from './user';
import { HttpClient } from '@angular/common/http';

@Injectable({
    // declares that this service should be created
    // by the root application injector.
    providedIn: 'root',
})
export class UserService {
    constructor(private http: HttpClient) { }

    getUsers() {
        const url = "https://jsonplaceholder.typicode.com/users/";
        return this.http.get<User[]>(url);
    }
}