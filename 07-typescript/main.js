"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var place_1 = require("./model/place");
var s = "stringa", // string
ss, // string
n = 1, // number
nn, // number
b = true, // boolean
bb, // boolean
o = { nome: "pippo", cognome: "pluto" }, // { nome: string, cognome: string }
u = { nome: "pippo", cognome: "pluto" }, // User
uu, // User
uuu = { nome: "pippo" }, // Partial<User>
a = [1, 2, 3], // number[]
aa; // number[]
console.info({
    s: s,
    ss: ss,
    n: n,
    nn: nn,
    b: b,
    bb: bb,
    o: o,
    u: u,
    uu: uu,
    uuu: uuu,
    a: a,
    aa: aa
});
console.info(new place_1.CoolPlace(2).getName());
