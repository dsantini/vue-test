import { User } from "./model/user";
import { CoolPlace } from "./model/place";

let s = "stringa", // string
    ss: string, // string
    n = 1, // number
    nn: number, // number
    b = true, // boolean
    bb: boolean, // boolean
    o = { nome: "pippo", cognome: "pluto" }, // { nome: string, cognome: string }
    u: User = { nome: "pippo", cognome: "pluto" }, // User
    uu: User, // User
    uuu: Partial<User> = { nome: "pippo" }, // Partial<User>
    a = [1, 2, 3], // number[]
    aa: number[]; // number[]

console.info({
    s, ss, n, nn, b, bb, o, u, uu, uuu, a, aa
});

console.info(new CoolPlace(2).getName())
