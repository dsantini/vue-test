export class OldStylePlace {
    private id: number;
    private name: string;
    constructor(id: number, name?: string) {
        this.id = id;
        this.name = name;
    }
    public getID() {
        return this.id;
    }
    public getName() {
        return this.name;
    }
}

export class CoolPlace {
    constructor(private id: number, private name?: number) {

    }
    public getID() {
        return this.id;
    }
    public getName() {
        return this.name;
    }
}
