"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoolPlace = exports.OldStylePlace = void 0;
var OldStylePlace = /** @class */ (function () {
    function OldStylePlace(id, name) {
        this.id = id;
        this.name = name;
    }
    OldStylePlace.prototype.getID = function () {
        return this.id;
    };
    OldStylePlace.prototype.getName = function () {
        return this.name;
    };
    return OldStylePlace;
}());
exports.OldStylePlace = OldStylePlace;
var CoolPlace = /** @class */ (function () {
    function CoolPlace(id, name) {
        this.id = id;
        this.name = name;
    }
    CoolPlace.prototype.getID = function () {
        return this.id;
    };
    CoolPlace.prototype.getName = function () {
        return this.name;
    };
    return CoolPlace;
}());
exports.CoolPlace = CoolPlace;
