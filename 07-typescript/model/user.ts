export interface Address {
    street: string,
    city: string
}

export type Role = "admin" | "editor"; // simil-enum

export interface User {
    nome: string,
    cognome: string,
    address?: Address,
    role?: Role
}